#include "nshTaskHandler.h"

#include "nshLog.h"

static bool g_handlerWindowClassRegistered = false;
static UINT g_shellHookMessage = WM_NULL;

#define NOTIFY_HEADER_MAGIC 0x34753423
#define TCDM_NOTIFY 1

struct TrayNotifyIconDataW32 {
	DWORD cbSize;
	DWORD dwWnd;
	UINT uID;
	UINT uFlags;
	UINT uCallbackMessage;
	DWORD dwIcon;
	WCHAR  szTip[128];
	DWORD dwState;
	DWORD dwStateMask;
	WCHAR  szInfo[256];
	union {
		UINT  uTimeout;
		UINT  uVersion;  // used with NIM_SETVERSION, values 0, 3 and 4
	} DUMMYUNIONNAME;
	WCHAR  szInfoTitle[64];
	DWORD dwInfoFlags;
	GUID guidItem;

	// Windows Vista and up
	DWORD dwBalloonIcon;
};

struct TrayNotifyIPCPacket {
	DWORD dwHeader;
	DWORD dwPacketID;
	TrayNotifyIconDataW32 nid;
};

void nshTaskHandler::CreateHandlerWindowClass() {
	WNDCLASSEXW wc = {};
	wc.cbSize = sizeof(wc);
	wc.hInstance = GetModuleHandleW(NULL);
	wc.lpszClassName = L"Shell_TrayWnd";
	wc.lpfnWndProc = &StaticWndProc;

	RegisterClassExW(&wc);
	g_handlerWindowClassRegistered = true;
}

LRESULT nshTaskHandler::StaticWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	nshTaskHandler* self;
	if (uMsg == WM_NCCREATE) {
		LPCREATESTRUCTW lpcs = reinterpret_cast<LPCREATESTRUCTW>(lParam);
		self = static_cast<nshTaskHandler*>(lpcs->lpCreateParams);
		self->m_hwnd = hwnd;
		SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(self));
	}
	else {
		self = reinterpret_cast<nshTaskHandler*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
	}
	if (self) {
		return self->WndProc(uMsg, wParam, lParam);
	}
	return DefWindowProcW(hwnd, uMsg, wParam, lParam);
}

BOOL nshTaskHandler::ProcessNotifyIcon(COPYDATASTRUCT* cpds) {
	if(cpds->cbData < offsetof(TrayNotifyIPCPacket, nid) + offsetof(TrayNotifyIconDataW32, dwBalloonIcon)) {
		return FALSE;
	}
	auto pkt = static_cast<TrayNotifyIPCPacket*>(cpds->lpData);
	if (pkt->dwHeader != NOTIFY_HEADER_MAGIC) return FALSE;
	auto nid = &pkt->nid;
	if (nid->cbSize < offsetof(TrayNotifyIconDataW32, dwBalloonIcon)) return FALSE;

	if (pkt->dwPacketID == NIM_ADD) {
		GR_INFO(L"Add tray icon with tooltip: %.*ls", 128, nid->szTip);
	} else if(pkt->dwPacketID == NIM_DELETE) {
		GR_INFO(L"Delete tray icon");
	} else if (pkt->dwPacketID == NIM_MODIFY) {
		GR_INFO(L"Modify tray icon");
	}

	return TRUE;
}

LRESULT nshTaskHandler::WndProc(UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (uMsg == g_shellHookMessage) {
		if (wParam == HSHELL_WINDOWDESTROYED) {
			HWND hwnd = (HWND) lParam;
			if (!IsWindow(hwnd)) return 0;
			if (!m_windowDestroyHandler) return 0;
			m_windowDestroyHandler(hwnd);

			return 0;
		}
		else if (wParam == HSHELL_WINDOWCREATED) {
			HWND hwnd = (HWND)lParam;
			if (!IsWindow(hwnd)) return 0;
			if (!m_windowCreateHandler) return 0;
			m_windowCreateHandler(hwnd);

			return 0;
		}
		else if (wParam == HSHELL_WINDOWACTIVATED || wParam == HSHELL_RUDEAPPACTIVATED) {
			HWND hwnd = (HWND)lParam;
			if (!IsWindow(hwnd)) return 0;
			if (!m_windowActivateHandler) return 0;
			m_windowActivateHandler(hwnd);

			return 0;
		}
		else {
			return 0;
		}
	}
	else if (uMsg == WM_PAINT) {
		PAINTSTRUCT ps;
		BeginPaint(m_hwnd, &ps);
		EndPaint(m_hwnd, &ps);

		return 0;
	}
	else if(uMsg == WM_COPYDATA) {
		COPYDATASTRUCT *cpds = reinterpret_cast<COPYDATASTRUCT*>(lParam);
		if (!cpds) return 0;
		if (cpds->dwData == TCDM_NOTIFY) {
			return ProcessNotifyIcon(cpds);
		}
		return FALSE;
	}
	else {
		return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
	}
}

nshTaskHandler::nshTaskHandler() {
	if (!g_handlerWindowClassRegistered) {
		CreateHandlerWindowClass();
	}
	if (g_shellHookMessage == WM_NULL) {
		g_shellHookMessage = RegisterWindowMessageW(L"SHELLHOOK");
	}

	CreateWindowExW(
		WS_EX_TOOLWINDOW | WS_EX_WINDOWEDGE,
		L"Shell_TrayWnd",
		L"Taskbar IPC Handler",
		WS_CLIPCHILDREN | WS_POPUP,
		-2000, -2000, 1, 1,
		NULL, NULL,
		GetModuleHandleW(NULL),
		this
	);

	m_modUser32 = LoadLibraryW(L"user32.dll");
	BOOL(WINAPI * SetShellWindow)(HWND) = (BOOL(WINAPI*)(HWND))GetProcAddress(m_modUser32, "SetShellWindow");
	BOOL(WINAPI * SetTaskmanWindow)(HWND) = (BOOL(WINAPI*)(HWND))GetProcAddress(m_modUser32, "SetTaskmanWindow");
	SetShellWindow(m_hwnd);
	SetTaskmanWindow(m_hwnd);

	MINIMIZEDMETRICS mmm;
	ZeroMemory(&mmm, sizeof(mmm));
	mmm.cbSize = sizeof(mmm);
	mmm.iArrange = ARW_HIDE;

	SystemParametersInfoW(SPI_SETMINIMIZEDMETRICS, sizeof(mmm), &mmm, SPIF_SENDWININICHANGE);
	SendMessage(m_hwnd, g_shellHookMessage, 0, 0);

	RegisterShellHookWindow(m_hwnd);

	SendNotifyMessage(HWND_BROADCAST, RegisterWindowMessage(TEXT("TaskbarCreated")), 0, 0);
}

nshTaskHandler::~nshTaskHandler() {
	DeregisterShellHookWindow(m_hwnd);

	DestroyWindow(m_hwnd);
}
