#pragma once

#include <wx/wx.h>

class nshApp : public wxApp {
public:
	bool OnInit() override;

	wxListBox* m_taskListBox;
};
