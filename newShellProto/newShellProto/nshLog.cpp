#include "nshLog.h"

#ifdef _WIN32
#include <windows.h>
#endif

#include <cstdio>
#include <cstdarg>

bool grLog_useWindowsDebugOutput = true;

void grLogMessage(const wchar_t* format, ...) {
    va_list args;
    va_list argscopy;
    va_start(args, format);
    va_copy(argscopy, args);
    int length = vswprintf(NULL, 0, format, args);
    va_end(args);
    if (length < 0) {
        va_end(argscopy);
        GR_FATAL(L"error logging message");
        return;
    }
    wchar_t* str = new wchar_t[length + 1];
    vswprintf(str, length + 1, format, argscopy);
    va_end(argscopy);
#ifdef _WIN32
    if (grLog_useWindowsDebugOutput) {
        OutputDebugStringW(str);
        OutputDebugStringW(L"\n");
    }
    else {
#endif
        fputws(str, stderr);
        fputwc(L'\n', stderr);
#ifdef _WIN32
    }
#endif
    delete[] str;
}
