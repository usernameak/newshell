#pragma once

#include <cstring>
#include <cstdio>
#include <cstdlib>

void grLogMessage(const wchar_t* format, ...);

#define GR_FATAL(...) do { \
    grLogMessage(L"[FATAL] " __VA_ARGS__); \
    abort();            \
} while(false)

#define GR_INFO(...) do { \
    grLogMessage(L"[INFO] " __VA_ARGS__); \
} while(false)

#define GR_WARNING(...) do { \
    grLogMessage(L"[WARN] " __VA_ARGS__); \
} while(false)
