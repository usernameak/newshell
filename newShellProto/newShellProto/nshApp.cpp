#include "nshApp.h"

#include "nshTaskFrame.h"

bool nshApp::OnInit() {
	nshTaskFrame* frame = new nshTaskFrame();
	frame->Show(true);
	
	return true;
}

wxIMPLEMENT_APP(nshApp);
