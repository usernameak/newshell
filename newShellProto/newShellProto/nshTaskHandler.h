#pragma once

#include <functional>

#include "nshWindows.h"

typedef void (*nshWindowEventHandler)(HWND hwnd);

class nshTaskHandler {
	HWND m_hwnd;
	HMODULE m_modUser32;
public:
	nshTaskHandler();
	~nshTaskHandler();

	std::function<void(HWND hwnd)> m_windowCreateHandler;
	std::function<void(HWND hwnd)> m_windowActivateHandler;
	std::function<void(HWND hwnd)> m_windowDestroyHandler;
private:
	static void CreateHandlerWindowClass();
	static LRESULT CALLBACK StaticWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	BOOL ProcessNotifyIcon(COPYDATASTRUCT* cpds);
	LRESULT CALLBACK WndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
};

