#pragma once

#include "nshWindows.h"

#include <wx/wx.h>

class nshTaskHandler;

class nshTaskFrame : public wxFrame {
public:
	nshTaskFrame();
	virtual ~nshTaskFrame();

	void OnClose(wxCloseEvent&);
	void OnTaskButtonClick(wxCommandEvent& evt);

	void OnWindowClosed(HWND hwnd);

	wxDECLARE_EVENT_TABLE();

	wxBoxSizer* m_sizer;
	nshTaskHandler* m_taskHandler;

	std::vector<wxBitmapButton*> m_buttons;

	void CheckInvalidWindows();
	void SetActiveWindowButton(wxBitmapButton* btn);

	wxBitmapButton* m_activeWindowButton;

private:
	HICON GetWindowIcon(HWND hwnd);
	BOOL AddWindow(HWND hwnd);
	static BOOL EnumWindowHandlerStatic(HWND hwnd, LPARAM lparam);
};

class nshTaskButtonData : public wxClientData {
public:
	HWND hWnd;
};
