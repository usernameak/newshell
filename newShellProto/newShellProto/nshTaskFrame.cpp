#include "nshTaskFrame.h"

#include <dwmapi.h>
#include "nshTaskHandler.h"

enum {
	nshID_TF_TASK_BUTTON = 1
};

wxBEGIN_EVENT_TABLE(nshTaskFrame, wxFrame)
	EVT_CLOSE(nshTaskFrame::OnClose)
	EVT_BUTTON(wxID_ANY, nshTaskFrame::OnTaskButtonClick)
wxEND_EVENT_TABLE()

nshTaskFrame::nshTaskFrame() : wxFrame(
	NULL, wxID_ANY,
	"Tasks",
	wxPoint{0, 0}, wxDefaultSize,
	wxCLIP_CHILDREN | wxFRAME_TOOL_WINDOW | wxSTAY_ON_TOP
), m_activeWindowButton(NULL) {
	m_taskHandler = new nshTaskHandler;
	m_taskHandler->m_windowCreateHandler = [this](HWND hwnd) {
		AddWindow(hwnd);
		Layout();
	};
	m_taskHandler->m_windowActivateHandler = [this](HWND hwnd) {
		CheckInvalidWindows();
		if(hwnd == NULL) {
			SetActiveWindowButton(NULL);
		} else {
			for (auto it = m_buttons.begin(); it != m_buttons.end(); ++it) {
				wxBitmapButton* btn = *it;
				nshTaskButtonData* btnData = dynamic_cast<nshTaskButtonData*>(btn->GetClientObject());
				if (!btnData) continue;
				if (btnData->hWnd == hwnd) {
					SetActiveWindowButton(btn);
					break;
				}
			}
		}
	};
	m_taskHandler->m_windowDestroyHandler = [this](HWND hwnd) { OnWindowClosed(hwnd); };

	m_sizer = new wxBoxSizer(wxVERTICAL);

	EnumWindows(&EnumWindowHandlerStatic, reinterpret_cast<LPARAM>(this));

	SetSizer(m_sizer);

	HMONITOR monitor = MonitorFromRect(NULL, MONITOR_DEFAULTTOPRIMARY);
	MONITORINFO monitorinfo;
	monitorinfo.cbSize = sizeof(MONITORINFO);
	GetMonitorInfoW(monitor, &monitorinfo);
	RECT rc = monitorinfo.rcMonitor;

	SetSize(rc.left, rc.top, 48, rc.bottom - rc.top);

	rc.left = GetSize().GetWidth();
	SystemParametersInfo(SPI_SETWORKAREA, TRUE, &rc, 0);
}

nshTaskFrame::~nshTaskFrame() {
	delete m_taskHandler;
}

void nshTaskFrame::OnClose(wxCloseEvent&) {

}

void nshTaskFrame::OnTaskButtonClick(wxCommandEvent& evt) {
	wxEvtHandler* evtHandler = wxDynamicCast(evt.GetEventObject(), wxEvtHandler);
	if (!evtHandler) return;
	nshTaskButtonData* btnData = dynamic_cast<nshTaskButtonData *>(evtHandler->GetClientObject());
	if (!btnData) return;
	if(IsIconic(btnData->hWnd)) {
		SendMessage(btnData->hWnd, WM_SYSCOMMAND, SC_RESTORE, -2);
	}
	SetForegroundWindow(btnData->hWnd);
}

void nshTaskFrame::OnWindowClosed(HWND hwnd) {
	for (auto it = m_buttons.begin(); it != m_buttons.end(); ++it) {
		wxBitmapButton* btn = *it;
		nshTaskButtonData* btnData = dynamic_cast<nshTaskButtonData*>(btn->GetClientObject());
		if (!btnData) continue;
		if (btnData->hWnd == hwnd) {
			if (m_activeWindowButton == btn) m_activeWindowButton = NULL;
			btn->Destroy();
			m_buttons.erase(it);
			Layout();
			break;
		}
	}
}

void nshTaskFrame::CheckInvalidWindows() {
	for (auto it = m_buttons.begin(); it != m_buttons.end();) {
		wxBitmapButton* btn = *it;
		nshTaskButtonData* btnData = dynamic_cast<nshTaskButtonData*>(btn->GetClientObject());
		if (!btnData) continue;
		if (!IsWindow(btnData->hWnd)) {
			if (m_activeWindowButton == btn) m_activeWindowButton = NULL;
			btn->Destroy();
			it = m_buttons.erase(it);
		} else {
			++it;
		}
	}
	Layout();
}

void nshTaskFrame::SetActiveWindowButton(wxBitmapButton* btn) {
	if(m_activeWindowButton) {
		m_activeWindowButton->SetBackgroundColour(wxColour{static_cast<unsigned long>(0x000000)});
	}
	m_activeWindowButton = btn;
	if (btn) {
		btn->SetBackgroundColour(wxColour{ 0xFF0000 });
	}
}

HICON nshTaskFrame::GetWindowIcon(HWND hWnd) {
	HICON hIcon;

	hIcon = (HICON)GetClassLongPtr(hWnd, GCLP_HICON);
	if (hIcon) {
		return hIcon;
	}

	hIcon = (HICON)SendMessage(hWnd, WM_GETICON, ICON_BIG, NULL);
	if (hIcon) {
		return hIcon;
	}

	hIcon = (HICON)GetClassLongPtr(hWnd, GCLP_HICONSM);
	if (hIcon) {
		return hIcon;
	}

	hIcon = (HICON)SendMessage(hWnd, WM_GETICON, ICON_SMALL, NULL);
	if (hIcon) {
		return hIcon;
	}

	return hIcon;
}

BOOL nshTaskFrame::AddWindow(HWND hwnd) {
	if (!IsWindowVisible(hwnd)) return TRUE;
	LONG lExStyle = GetWindowLongW(hwnd, GWL_EXSTYLE);
	if (GetWindow(hwnd, GW_OWNER) && !(lExStyle & WS_EX_APPWINDOW)) return TRUE;
	if (lExStyle & WS_EX_TOOLWINDOW) return TRUE;

	BOOL isCloaked = FALSE;
	if (SUCCEEDED(DwmGetWindowAttribute(hwnd, DWMWA_CLOAKED, &isCloaked, sizeof(isCloaked))) && isCloaked) return TRUE;

	const int titleLength = GetWindowTextLengthW(hwnd);
	auto title = new WCHAR[titleLength + 1];
	if (!GetWindowTextW(hwnd, title, titleLength + 1)) return TRUE;

	HICON hIcon = GetWindowIcon(hwnd);
	wxIcon ico;
	ico.CreateFromHICON(hIcon);
	wxBitmap bmp;
	bmp.CopyFromIcon(ico);

	auto tbd = new nshTaskButtonData;
	tbd->hWnd = hwnd;

	auto btn = new wxBitmapButton(this, wxID_ANY, bmp);
	btn->SetToolTip(title);
	btn->SetClientObject(tbd);
	if(GetForegroundWindow() == hwnd) {
		SetActiveWindowButton(btn);
	} else {
		btn->SetBackgroundColour(wxColour{static_cast<unsigned long>(0x000000)});
	}
	m_sizer->Add(btn, 0, wxEXPAND);

	m_buttons.push_back(btn);

	delete[] title;

	return TRUE;
}

BOOL nshTaskFrame::EnumWindowHandlerStatic(HWND hwnd, LPARAM lparam) {
	return reinterpret_cast<nshTaskFrame*>(lparam)->AddWindow(hwnd);
}

